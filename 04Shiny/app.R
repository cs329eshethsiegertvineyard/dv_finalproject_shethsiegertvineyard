library(shiny)
require(shinydashboard)
require(leaflet)
require("jsonlite")
require("RCurl")
require(ggplot2)
require(dplyr)
require(DT)
require(treemap)
library(reshape2)
library(scales)

ui <- dashboardPage(
  dashboardHeader(title = "Inequality in the American Healthcare System", titleWidth = 600),
  dashboardSidebar(
    sidebarMenu(
      menuItem("Medical Advances Inequality",tabName="codrace",icon=icon("dashboard")),
      menuItem("Cause Inequality by Race",tabName="codrel",icon=icon("dashboard")),
      menuItem("Cause by Education",tabName="codeduc",icon=icon("dashboard")),
      menuItem("Cancer Inequality by Race",tabName="cancerrace",icon=icon("dashboard")),
      menuItem("Married Treatment Difference",tabName="ineqMar",icon=icon("dashboard"))
    )
  ),
  dashboardBody(
    tabItems(
      tabItem(tabName="codrace",
              actionButton(inputId="bl",label="African-American"),
              actionButton(inputId="wh",label="White"),
              actionButton(inputId="ot",label="Other Races"),
              plotOutput("causeTime")
            #  plotOutput("causeTime2"),
            #  plotOutput("causeTime3")
            ),
      tabItem(tabName="codrel",
              plotOutput("causeRelative")),
      tabItem(tabName="codeduc",
              actionButton(inputId="year5",label="2005"),
              actionButton(inputId="year10",label="2010"),
              actionButton(inputId="year14",label="2014"),
              plotOutput("causeEduc")
            #  plotOutput("causeEduc2"),
            #  plotOutput("causeEduc3")
            ),
      tabItem(tabName="cancerrace",
              plotOutput("cancerRace")),
      tabItem(tabName="ineqMar",
              actionButton(inputId="y5",label="2005"),
              actionButton(inputId="y10",label="2010"),
              actionButton(inputId="y14",label="2014"),
              plotOutput("marCrosstab")
           #   plotOutput("marCrosstab2"),
           #   plotOutput("marCrosstab3")
           )
    )
  )
)

server <- shinyServer(function(input,output){
   df2005 <- data.frame(fromJSON(getURL(URLencode(gsub("\n", " ", 'oraclerest.cs.utexas.edu:5001/rest/native/?query="select * from SHORTMORT2005" ')), httpheader=c(DB='jdbc:oracle:thin:@aevum.cs.utexas.edu:1521/f16pdb', USER = 'cs329e_jfv334', PASS = 'orcl_jfv334', MODE='native_mode', MODEL='model', returnDimensions = 'False', returnFor = 'JSON'),verbose=TRUE)))
  # 
   #View(df2005)
  # 
   df2010 <- data.frame(fromJSON(getURL(URLencode(gsub("\n", " ", 'oraclerest.cs.utexas.edu:5001/rest/native/?query="select * from SHORTMORT2010" ')), httpheader=c(DB='jdbc:oracle:thin:@aevum.cs.utexas.edu:1521/f16pdb', USER = 'cs329e_jfv334', PASS = 'orcl_jfv334', MODE='native_mode', MODEL='model', returnDimensions = 'False', returnFor = 'JSON'),verbose=TRUE)))
  # 
  # View(df2010)
  # 
   df2014 <- data.frame(fromJSON(getURL(URLencode(gsub("\n", " ", 'oraclerest.cs.utexas.edu:5001/rest/native/?query="select * from SHORTMORT2014" ')), httpheader=c(DB='jdbc:oracle:thin:@aevum.cs.utexas.edu:1521/f16pdb', USER = 'cs329e_jfv334', PASS = 'orcl_jfv334', MODE='native_mode', MODEL='model', returnDimensions = 'False', returnFor = 'JSON'),verbose=TRUE)))
  # 
  # #View(df2014)
  
  
  # Temporary Fix
  setwd("~/FALL 2016/C S 329E/DataVisualization/DV_Final_Project_v1/01 Data")
  mortal05 <- read.csv("shortmort2005.csv",stringsAsFactors = FALSE) #%>% View
  mortal10 <- read.csv("shortmort2010.csv",stringsAsFactors = FALSE)
  mortal14 <- read.csv("shortmort2014.csv",stringsAsFactors = FALSE) #%>% View
  
  
  # Include bubble plot code here (from Project 6)
  
  # Include intro smoking plot here (from older data sets)
  
  
  ## Cause of Death for a Single Race Across Years - Dodged Bars - Good
  
  # dfBlack2005 <- df2005 %>% filter(RACE==2) %>% dplyr::mutate(SL = substr(UCOD,1,1)) %>% group_by(SL) %>% summarize(N05B=n()) 
  # 
  # dfBlack2010 <- df2010 %>% filter(RACE==2) %>% dplyr::mutate(SL = substr(UCOD,1,1)) %>% group_by(SL) %>% summarize(N10B=n()) 
  # 
  # dfBlack2014 <- df2014 %>% filter(RACE==2) %>% dplyr::mutate(SL = substr(UCOD,1,1)) %>% group_by(SL) %>% summarize(N14B=n()) #%>% View
  
  dfBlack2005 <- df2005 %>% filter(RACE==2) %>% dplyr::mutate(SL = substr(UCOD,1,1)) %>% dplyr::mutate(ICDNAME = ifelse(SL=="A","Infections",ifelse(SL=="B","Infections",ifelse(SL=="C","Cancer",ifelse(SL=="D","Cancer",ifelse(SL=="E","Metabolic",ifelse(SL=="F","Mental Disorders",ifelse(SL=="G","Nervous",ifelse(SL=="H","Eye",ifelse(SL=="I","Circulatory",ifelse(SL=="J","Respiratory",ifelse(SL=="K","Digestive",ifelse(SL=="L","Skin",ifelse(SL=="M","Muscle/Bone",ifelse(SL=="N","Genitourinary",ifelse(SL=="O","Pregnancy",ifelse(SL=="P","Birth Defects",ifelse(SL=="Q", "Birth Defects",ifelse(SL=="R","Miscellaneous",ifelse(SL=="S","External Causes",ifelse(SL=="T","External Causes", ifelse(SL=="U","Miscellaneous",ifelse(SL=="V","External Causes",ifelse(SL=="W","External Causes",ifelse(SL=="X","External Causes",ifelse(SL=="Y","External Causes","Miscellaneous")))))))))))))))))))))))))) %>% group_by(ICDNAME) %>% summarize(N05B=n()) #%>% View
  
  dfBlack2010 <- df2010 %>% filter(RACE==2) %>% dplyr::mutate(SL = substr(UCOD,1,1)) %>% dplyr::mutate(ICDNAME = ifelse(SL=="A","Infections",ifelse(SL=="B","Infections",ifelse(SL=="C","Cancer",ifelse(SL=="D","Cancer",ifelse(SL=="E","Metabolic",ifelse(SL=="F","Mental Disorders",ifelse(SL=="G","Nervous",ifelse(SL=="H","Eye",ifelse(SL=="I","Circulatory",ifelse(SL=="J","Respiratory",ifelse(SL=="K","Digestive",ifelse(SL=="L","Skin",ifelse(SL=="M","Muscle/Bone",ifelse(SL=="N","Genitourinary",ifelse(SL=="O","Pregnancy",ifelse(SL=="P","Birth Defects",ifelse(SL=="Q", "Birth Defects",ifelse(SL=="R","Miscellaneous",ifelse(SL=="S","External Causes",ifelse(SL=="T","External Causes", ifelse(SL=="U","Miscellaneous",ifelse(SL=="V","External Causes",ifelse(SL=="W","External Causes",ifelse(SL=="X","External Causes",ifelse(SL=="Y","External Causes","Miscellaneous")))))))))))))))))))))))))) %>% group_by(ICDNAME) %>% summarize(N10B=n())
  
  dfBlack2014 <- df2014 %>% filter(RACE==2) %>% dplyr::mutate(SL = substr(UCOD,1,1)) %>% dplyr::mutate(ICDNAME = ifelse(SL=="A","Infections",ifelse(SL=="B","Infections",ifelse(SL=="C","Cancer",ifelse(SL=="D","Cancer",ifelse(SL=="E","Metabolic",ifelse(SL=="F","Mental Disorders",ifelse(SL=="G","Nervous",ifelse(SL=="H","Eye",ifelse(SL=="I","Circulatory",ifelse(SL=="J","Respiratory",ifelse(SL=="K","Digestive",ifelse(SL=="L","Skin",ifelse(SL=="M","Muscle/Bone",ifelse(SL=="N","Genitourinary",ifelse(SL=="O","Pregnancy",ifelse(SL=="P","Birth Defects",ifelse(SL=="Q", "Birth Defects",ifelse(SL=="R","Miscellaneous",ifelse(SL=="S","External Causes",ifelse(SL=="T","External Causes", ifelse(SL=="U","Miscellaneous",ifelse(SL=="V","External Causes",ifelse(SL=="W","External Causes",ifelse(SL=="X","External Causes",ifelse(SL=="Y","External Causes","Miscellaneous")))))))))))))))))))))))))) %>% group_by(ICDNAME) %>% summarize(N14B=n())
  
  dfBlackComb1 <- dplyr::inner_join(dfBlack2005,dfBlack2010,by="ICDNAME")
  dfBlackComb2 <- dplyr::inner_join(dfBlackComb1,dfBlack2014,by="ICDNAME") #%>% View
  
 # mdfBlack <- melt(dfBlackComb2, id="ICDNAME") #%>% View
  
  mdfBlack <- melt(dfBlackComb2, id="ICDNAME") %>% dplyr::mutate(P = ifelse(variable=="N05B",value/2065,ifelse(variable=="N10B",value/1151,value/1223))) #%>% View
  
  
  dfWhite2005 <- df2005 %>% filter(RACE==1) %>% dplyr::mutate(SL=substr(UCOD,1,1)) %>% dplyr::mutate(ICDNAME = ifelse(SL=="A","Infections",ifelse(SL=="B","Infections",ifelse(SL=="C","Cancer",ifelse(SL=="D","Cancer",ifelse(SL=="E","Metabolic",ifelse(SL=="F","Mental Disorders",ifelse(SL=="G","Nervous",ifelse(SL=="H","Eye",ifelse(SL=="I","Circulatory",ifelse(SL=="J","Respiratory",ifelse(SL=="K","Digestive",ifelse(SL=="L","Skin",ifelse(SL=="M","Muscle/Bone",ifelse(SL=="N","Genitourinary",ifelse(SL=="O","Pregnancy",ifelse(SL=="P","Birth Defects",ifelse(SL=="Q", "Birth Defects",ifelse(SL=="R","Miscellaneous",ifelse(SL=="S","External Causes",ifelse(SL=="T","External Causes", ifelse(SL=="U","Miscellaneous",ifelse(SL=="V","External Causes",ifelse(SL=="W","External Causes",ifelse(SL=="X","External Causes",ifelse(SL=="Y","External Causes","Miscellaneous")))))))))))))))))))))))))) %>% group_by(ICDNAME) %>% summarize(N05W=n())
  
  dfWhite2010 <- df2010 %>% filter(RACE==1) %>% dplyr::mutate(SL=substr(UCOD,1,1)) %>% dplyr::mutate(ICDNAME = ifelse(SL=="A","Infections",ifelse(SL=="B","Infections",ifelse(SL=="C","Cancer",ifelse(SL=="D","Cancer",ifelse(SL=="E","Metabolic",ifelse(SL=="F","Mental Disorders",ifelse(SL=="G","Nervous",ifelse(SL=="H","Eye",ifelse(SL=="I","Circulatory",ifelse(SL=="J","Respiratory",ifelse(SL=="K","Digestive",ifelse(SL=="L","Skin",ifelse(SL=="M","Muscle/Bone",ifelse(SL=="N","Genitourinary",ifelse(SL=="O","Pregnancy",ifelse(SL=="P","Birth Defects",ifelse(SL=="Q", "Birth Defects",ifelse(SL=="R","Miscellaneous",ifelse(SL=="S","External Causes",ifelse(SL=="T","External Causes", ifelse(SL=="U","Miscellaneous",ifelse(SL=="V","External Causes",ifelse(SL=="W","External Causes",ifelse(SL=="X","External Causes",ifelse(SL=="Y","External Causes","Miscellaneous")))))))))))))))))))))))))) %>% group_by(ICDNAME) %>% summarize(N10W=n())
  
  dfWhite2014 <- df2014 %>% filter(RACE==1) %>% dplyr::mutate(SL=substr(UCOD,1,1)) %>% dplyr::mutate(ICDNAME = ifelse(SL=="A","Infections",ifelse(SL=="B","Infections",ifelse(SL=="C","Cancer",ifelse(SL=="D","Cancer",ifelse(SL=="E","Metabolic",ifelse(SL=="F","Mental Disorders",ifelse(SL=="G","Nervous",ifelse(SL=="H","Eye",ifelse(SL=="I","Circulatory",ifelse(SL=="J","Respiratory",ifelse(SL=="K","Digestive",ifelse(SL=="L","Skin",ifelse(SL=="M","Muscle/Bone",ifelse(SL=="N","Genitourinary",ifelse(SL=="O","Pregnancy",ifelse(SL=="P","Birth Defects",ifelse(SL=="Q", "Birth Defects",ifelse(SL=="R","Miscellaneous",ifelse(SL=="S","External Causes",ifelse(SL=="T","External Causes", ifelse(SL=="U","Miscellaneous",ifelse(SL=="V","External Causes",ifelse(SL=="W","External Causes",ifelse(SL=="X","External Causes",ifelse(SL=="Y","External Causes","Miscellaneous")))))))))))))))))))))))))) %>% group_by(ICDNAME) %>% summarize(N14W=n()) #%>% View
  
  dfWhiteComb1 <- dplyr::inner_join(dfWhite2005,dfWhite2010,by="ICDNAME")
  dfWhiteComb2 <- dplyr::inner_join(dfWhiteComb1,dfWhite2014,by="ICDNAME") #%>% View
  
  mdfWhite <- melt(dfWhiteComb2,id="ICDNAME") %>% dplyr::mutate(P = ifelse(variable=="N05W",value/8264,ifelse(variable=="N10W",value/8903,value/8826))) #%>% View
  
  
  dfOther2005 <- df2005 %>% filter(RACE!=1) %>% filter(RACE!=2) %>% dplyr::mutate(SL=substr(UCOD,1,1)) %>% dplyr::mutate(ICDNAME = ifelse(SL=="A","Infections",ifelse(SL=="B","Infections",ifelse(SL=="C","Cancer",ifelse(SL=="D","Cancer",ifelse(SL=="E","Metabolic",ifelse(SL=="F","Mental Disorders",ifelse(SL=="G","Nervous",ifelse(SL=="H","Eye",ifelse(SL=="I","Circulatory",ifelse(SL=="J","Respiratory",ifelse(SL=="K","Digestive",ifelse(SL=="L","Skin",ifelse(SL=="M","Muscle/Bone",ifelse(SL=="N","Genitourinary",ifelse(SL=="O","Pregnancy",ifelse(SL=="P","Birth Defects",ifelse(SL=="Q", "Birth Defects",ifelse(SL=="R","Miscellaneous",ifelse(SL=="S","External Causes",ifelse(SL=="T","External Causes", ifelse(SL=="U","Miscellaneous",ifelse(SL=="V","External Causes",ifelse(SL=="W","External Causes",ifelse(SL=="X","External Causes",ifelse(SL=="Y","External Causes","Miscellaneous")))))))))))))))))))))))))) %>% group_by(ICDNAME) %>% summarize(N05O=n())
  
  dfOther2010 <- df2010 %>% filter(RACE!=1) %>% filter(RACE!=2) %>% dplyr::mutate(SL=substr(UCOD,1,1)) %>% dplyr::mutate(ICDNAME = ifelse(SL=="A","Infections",ifelse(SL=="B","Infections",ifelse(SL=="C","Cancer",ifelse(SL=="D","Cancer",ifelse(SL=="E","Metabolic",ifelse(SL=="F","Mental Disorders",ifelse(SL=="G","Nervous",ifelse(SL=="H","Eye",ifelse(SL=="I","Circulatory",ifelse(SL=="J","Respiratory",ifelse(SL=="K","Digestive",ifelse(SL=="L","Skin",ifelse(SL=="M","Muscle/Bone",ifelse(SL=="N","Genitourinary",ifelse(SL=="O","Pregnancy",ifelse(SL=="P","Birth Defects",ifelse(SL=="Q", "Birth Defects",ifelse(SL=="R","Miscellaneous",ifelse(SL=="S","External Causes",ifelse(SL=="T","External Causes", ifelse(SL=="U","Miscellaneous",ifelse(SL=="V","External Causes",ifelse(SL=="W","External Causes",ifelse(SL=="X","External Causes",ifelse(SL=="Y","External Causes","Miscellaneous")))))))))))))))))))))))))) %>% group_by(ICDNAME) %>% summarize(N10O=n())
  
  dfOther2014 <- df2014 %>% filter(RACE!=1) %>% filter(RACE!=2) %>% dplyr::mutate(SL=substr(UCOD,1,1)) %>% dplyr::mutate(ICDNAME = ifelse(SL=="A","Infections",ifelse(SL=="B","Infections",ifelse(SL=="C","Cancer",ifelse(SL=="D","Cancer",ifelse(SL=="E","Metabolic",ifelse(SL=="F","Mental Disorders",ifelse(SL=="G","Nervous",ifelse(SL=="H","Eye",ifelse(SL=="I","Circulatory",ifelse(SL=="J","Respiratory",ifelse(SL=="K","Digestive",ifelse(SL=="L","Skin",ifelse(SL=="M","Muscle/Bone",ifelse(SL=="N","Genitourinary",ifelse(SL=="O","Pregnancy",ifelse(SL=="P","Birth Defects",ifelse(SL=="Q", "Birth Defects",ifelse(SL=="R","Miscellaneous",ifelse(SL=="S","External Causes",ifelse(SL=="T","External Causes", ifelse(SL=="U","Miscellaneous",ifelse(SL=="V","External Causes",ifelse(SL=="W","External Causes",ifelse(SL=="X","External Causes",ifelse(SL=="Y","External Causes","Miscellaneous")))))))))))))))))))))))))) %>% group_by(ICDNAME) %>% summarize(N14O=n())
  
  dfOtherComb1 <- dplyr::inner_join(dfOther2005,dfOther2010,by="ICDNAME")
  dfOtherComb2 <- dplyr::inner_join(dfOtherComb1,dfOther2014,by="ICDNAME") 
  
  mdfOther <- melt(dfOtherComb2,id="ICDNAME") %>% dplyr::mutate(P = ifelse(variable=="N05O",value/156,ifelse(variable=="N10O",value/431,value/436))) #%>% View
  
  
  observeEvent(input$bl,{
  output$causeTime <- renderPlot({
    ctB <- ggplot() +
      coord_cartesian() +
      scale_fill_discrete(labels=c("2005","2010","2014")) +
      labs(x="Cause of Death Category",y="Proportion of Deaths") +
      labs(title="Time Series of Causes of Death for African-Americans") +
      labs(fill="Year") +
      theme_gray() +
      layer(data=mdfBlack,
            geom="bar",
            #mapping = aes(x=ICDNAME,y=value,fill=variable),
            mapping = aes(x=ICDNAME,y=P,fill=variable),
            #mapping = aes(x=SL,y=ifelse(YEAR==2005,N05B,ifelse(YEAR==2010,N10B,N14B))),
            stat = "identity",
            position = "dodge")
    ctB
  })
  })
   
  observeEvent(input$wh,{
 # output$causeTime2 <- renderPlot({ 
    output$causeTime <- renderPlot({ 
    ctW <- ggplot() +
      coord_cartesian() +
      scale_fill_discrete(labels=c("2005","2010","2014")) +
      labs(x="Cause of Death Category",y="Proportion of Deaths") +
      labs(title="Time Series of Causes of Death for Whites") +
      labs(fill="Year") +
      theme_gray() +
      layer(data=mdfWhite,
            geom="bar",
            #mapping = aes(x=ICDNAME,y=value,fill=variable),
            mapping = aes(x=ICDNAME,y=P,fill=variable),
            #mapping = aes(x=SL,y=ifelse(YEAR==2005,N05B,ifelse(YEAR==2010,N10B,N14B))),
            stat = "identity",
            position = "dodge")
    ctW
  })
  })
   
  observeEvent(input$ot,{ 
 # output$causeTime3 <- renderPlot({
    output$causeTime <- renderPlot({ 
    ctO <- ggplot() +
      coord_cartesian() +
      scale_fill_discrete(labels=c("2005","2010","2014")) +
      labs(x="Cause of Death Category",y="Proportion of Deaths") +
      labs(title="Time Series of Causes of Death for Other Races") +
      labs(fill="Year") +
      theme_gray() +
      layer(data=mdfOther,
            geom="bar",
            #mapping = aes(x=ICDNAME,y=value,fill=variable),
            mapping = aes(x=ICDNAME,y=P,fill=variable),
            #mapping = aes(x=SL,y=ifelse(YEAR==2005,N05B,ifelse(YEAR==2010,N10B,N14B))),
            stat = "identity",
            position = "dodge")
    ctO
  })
  })
  
  
  
  ## Comparing Cause of Death for Two Races for a Single Year - Positive/Negative Bars - Good
  
  dfCompare1 <- dplyr::inner_join(dfWhite2014,dfBlack2014,by="ICDNAME") #%>% View #%>% dplyr::mutate(whitePerc = n14w/(n14w+n14b+n14o)) %>% View
  
  dfCompare2 <- dplyr::inner_join(dfCompare1,dfOther2014,by="ICDNAME") %>% dplyr::mutate(WHITEPERC = N14W/(N14W+N14B+N14O)) %>% dplyr::mutate(BLACKPERC = N14B/(N14W+N14B+N14O)) %>% dplyr::mutate(WHITEDIFF = WHITEPERC-0.8418) %>% dplyr::mutate(BLACKDIFF = BLACKPERC-0.1164) #%>% View
  
  #%>% View #%>% dplyr::rename(dfCompare1, x = whitePerc)#%>% dplyr::rename(dfCompare1,n14.y = blackPerc) %>% View
  #dfCompare2 <- dfCompare1 %>% dplyr::rename(dfCompare1,n14.x=whitePerc) %>% View
  
  output$causeRelative <- renderPlot({
    rel <- ggplot() +
      coord_cartesian() +
      #scale_fill_manual(values=c("blue","orange")) +
      scale_fill_manual(values=c("Orange","Blue"),labels=c("African-American","White")) +
      labs(x="Cause of Death Category",y="Proportion Difference from Expected Value") +
      labs(title="Inequalities in Treatment of Various Conditions") +
      labs(fill="Race") +
      layer(data=dfCompare2,
            geom="bar",
            mapping=aes(x=ICDNAME,y=WHITEDIFF,fill="#FF9999"),
            stat="identity",
            position=position_identity()) +
      layer(data=dfCompare2,
            geom="bar",
            mapping=aes(x=ICDNAME,y=BLACKDIFF,fill="#BE5555"),
            stat="identity",
            position=position_identity())
    rel
  })
  
  
  # Inequality with Respect to Education - Linegraph with Color - good
  
  
  #dfEduc2005 <- mortal05 %>% dplyr::mutate(SL = substr(ucod,1,1)) %>% dplyr::mutate(circ=ifelse(SL=="I",1,0)) %>% dplyr::mutate(educReformat = round(educ89/2,0)) %>% dplyr::mutate(ageFirst = as.numeric(as.character(substr(as.character(age),1,1)))) %>% dplyr::mutate(ageEnd = as.numeric(as.character(substr(as.character(age),2,4)))) %>% dplyr::mutate(correctAge = ifelse(ageFirst==1,ageEnd,ifelse(ageFirst==2,ageEnd/12,ifelse(ageFirst==4,ageEnd/365,0)))) %>% filter(educReformat<=18) %>% group_by(educReformat) %>% summarize(circPerc = mean(circ),avgA05 = mean(correctAge)) %>% View
  
  dfEduc2005 <- df2005 %>% dplyr::mutate(SL = substr(UCOD,1,1)) %>% dplyr::mutate(CIRC=ifelse(SL=="I",1,0)) %>% dplyr::mutate(AGEFIRST = as.numeric(as.character(substr(as.character(AGE),1,1)))) %>% dplyr::mutate(AGEEND = as.numeric(as.character(substr(as.character(AGE),2,4)))) %>% dplyr::mutate(CORRECTAGE = ifelse(AGEFIRST==1,AGEEND,ifelse(AGEFIRST==2,AGEEND/12,ifelse(AGEFIRST==4,AGEEND/365,0)))) %>% filter(EDUC89!=0) %>% filter(EDUC89<=18) %>% dplyr::mutate(EDUCREFORMAT = ifelse(as.numeric(as.character(EDUC89))<9, "01 <8th Grade", ifelse(as.numeric(as.character(EDUC89))<12, "02 Less than Diploma",ifelse(as.numeric(as.character(EDUC89))<16,"03 Some college","04 College Grad")))) %>% group_by(EDUCREFORMAT) %>% summarize(CIRCPERC = mean(CIRC),AVGA05 = mean(CORRECTAGE)) #%>% View
  
  dfEduc2010 <- df2010 %>% dplyr::mutate(SL = substr(UCOD,1,1)) %>% dplyr::mutate(CIRC=ifelse(SL=="I",1,0)) %>% dplyr::mutate(AGEFIRST = as.numeric(as.character(substr(as.character(AGE),1,1)))) %>% dplyr::mutate(AGEEND = as.numeric(as.character(substr(as.character(AGE),2,4)))) %>% dplyr::mutate(CORRECTAGE = ifelse(AGEFIRST==1,AGEEND,ifelse(AGEFIRST==2,AGEEND/12,ifelse(AGEFIRST==4,AGEEND/365,0)))) %>% filter(EDUC2003!=0) %>% filter(EDUC2003!=9) %>% dplyr::mutate(EDUC = ifelse(as.numeric(as.character(EDUC2003))==1, "01 Less than HS", ifelse(as.numeric(as.character(EDUC2003))==2, "02 No diploma", ifelse(as.numeric(as.character(EDUC2003))==3, "03 HS Diploma", ifelse(as.numeric(as.character(EDUC2003))==4, "04 Some college", ifelse(as.numeric(as.character(EDUC2003))==5, "05 Associate's", ifelse(as.numeric(as.character(EDUC2003))==6, "06 Bachelor's", ifelse(as.numeric(as.character(EDUC2003))==7, "07 Master's", "08 Doctorate")))))))) %>% group_by(EDUC) %>% summarize(CIRCPERC = mean(CIRC),AVGA10 = mean(CORRECTAGE)) #%>% View
  
  dfEduc2014 <- df2014 %>% dplyr::mutate(SL = substr(UCOD,1,1)) %>% dplyr::mutate(CIRC = ifelse(SL=="I",1,0)) %>% filter(EDUC2003!=0) %>% filter(EDUC2003!=9) %>% dplyr::mutate(EDUC = ifelse(as.numeric(as.character(EDUC2003))==1, "01 Less than HS", ifelse(as.numeric(as.character(EDUC2003))==2, "02 No diploma", ifelse(as.numeric(as.character(EDUC2003))==3, "03 HS Diploma", ifelse(as.numeric(as.character(EDUC2003))==4, "04 Some college", ifelse(as.numeric(as.character(EDUC2003))==5, "05 Associate's", ifelse(as.numeric(as.character(EDUC2003))==6, "06 Bachelor's", ifelse(as.numeric(as.character(EDUC2003))==7, "07 Master's", "08 Doctorate")))))))) %>% group_by(EDUC) %>% dplyr::mutate(AGEFIRST = as.numeric(as.character(substr(as.character(AGE),1,1)))) %>% dplyr::mutate(AGEEND = as.numeric(as.character(substr(as.character(AGE),2,4)))) %>% dplyr::mutate(CORRECTAGE = ifelse(AGEFIRST==1,AGEEND,ifelse(AGEFIRST==2,AGEEND/12,ifelse(AGEFIRST==4,AGEEND/365,0)))) %>% summarize(CIRCPERC = mean(CIRC),AVGA14 = mean(CORRECTAGE)) #%>% View 
  
  observeEvent(input$year5,{ 
  output$causeEduc <- renderPlot({
    # ce <- ggplot() +
    #   coord_cartesian(ylim=c(0.275,0.4)) +
    #   scale_colour_gradient(low="green", high="red") +
    #   labs(x="Level of Education",y="Proportion of Deaths due to Circulatory Issues") +
    #   labs(title="Inequality in Treatment due to Education - 2005") +
    #   labs(color="Average Age at Death") +
    #   layer(data=dfEduc2005,
    #      # geom="line",
    #       geom = "line",
    #       mapping=aes(x=EDUCREFORMAT,y=CIRCPERC,color=AVGA05,group=1,size=5),
    #       stat="identity",
    #       position=position_identity())
    # ce
    
    ce <- ggplot(data=dfEduc2005, aes(x=EDUCREFORMAT, y=CIRCPERC, group=1, color=AVGA05)) + 
      geom_line(size=1.25) + labs(x="Level of Education",y="Proportion of Deaths due to Circulatory Issues") + labs(title="Inequality in Treatment due to Education - 2005") + labs(color="Average Age at Death") + scale_colour_gradient(limits=c(68,80),low="red", high="green4") + coord_cartesian(ylim=c(0.275,0.4))
    ce
  })
  })
  
  observeEvent(input$year10,{ 
  output$causeEduc <- renderPlot({
    # ce2 <- ggplot() +
    #   coord_cartesian(ylim=c(0.275,0.4)) +
    #   scale_colour_gradient(low="green", high="red") + 
    #   labs(x="Level of Education",y="Proportion of Deaths due to Circulatory Issues") +
    #   labs(title="Inequality in Treatment due to Education - 2010") +
    #   labs(color="Average Age at Death") +
    #   layer(data=dfEduc2010,
    #         geom="line",
    #         mapping=aes(x=EDUC,y=CIRCPERC,color=AVGA10,group=1),
    #         stat="identity",
    #         position=position_identity())
    # ce2
    
    ce2 <- ggplot(data=dfEduc2010, aes(x=EDUC, y=CIRCPERC, group=1, color=AVGA10)) + 
      geom_line(size=1.25) + labs(x="Level of Education",y="Proportion of Deaths due to Circulatory Issues") + labs(title="Inequality in Treatment due to Education - 2010") + labs(color="Average Age at Death") + scale_colour_gradient(limits=c(68,80),low="red", high="green4") + coord_cartesian(ylim=c(0.275,0.4))
    ce2
  })
  })
  
  observeEvent(input$year14,{ 
  output$causeEduc <- renderPlot({
    # ce3 <- ggplot() +
    #   coord_cartesian(ylim=c(0.275,0.4)) +
    #   scale_colour_gradient(low="green", high="red") + 
    #   labs(x="Level of Education",y="Proportion of Deaths due to Circulatory Issues") +
    #   labs(title="Inequality in Treatment due to Education - 2014") +
    #   labs(color="Average Age at Death") +
    #   layer(data=dfEduc2014,
    #         geom="line",
    #         mapping=aes(x=EDUC,y=CIRCPERC,color=AVGA14,group=1),
    #         stat="identity",
    #         position=position_identity())
    # ce3
    
    ce3 <- ggplot(data=dfEduc2014, aes(x=EDUC, y=CIRCPERC, group=1, color=AVGA14)) + 
      geom_line(size=1.25) + labs(x="Level of Education",y="Proportion of Deaths due to Circulatory Issues") + labs(title="Inequality in Treatment due to Education - 2014") + labs(color="Average Age at Death") + scale_colour_gradient(limits=c(68,80),low="red", high="green4") + coord_cartesian(ylim=c(0.275,0.4))
    ce3
  })
  })
  
  
  # Showing that some races' cancer average age at death changed, while some didn't
  # Figure out how to do average age here - good
  
  #dfCan2005 <- df2005 %>% dplyr::mutate(SL = substr(UCOD,1,1)) %>% dplyr::mutate(CAN = ifelse(SL=="C",1,ifelse(SL=="D",1,0))) %>% dplyr::mutate(AGEFIRST = as.numeric(as.character(substr(as.character(AGE),1,1)))) %>% dplyr::mutate(AGEEND = as.numeric(as.character(substr(as.character(AGE),2,4)))) %>% dplyr::mutate(CORRECTAGE = ifelse(AGEFIRST==1,AGEEND,ifelse(AGEFIRST==2,AGEEND/12,ifelse(AGEFIRST==4,AGEEND/365,0)))) %>% dplyr::mutate(RENAMERACE = ifelse(RACE==1,"White",ifelse(RACE==2,"Black",ifelse(RACE==3,"Native American","Asian")))) %>% group_by(RENAMERACE) %>% summarize(CP05 = mean(CAN),AVGAGE05 = mean(CORRECTAGE)) #%>% View
  
  dfCan2005 <- df2005 %>% dplyr::mutate(SL = substr(UCOD,1,1)) %>% dplyr::mutate(CAN = ifelse(SL=="C",1,ifelse(SL=="D",1,0))) %>% dplyr::mutate(AGEFIRST = as.numeric(as.character(substr(as.character(AGE),1,1)))) %>% dplyr::mutate(AGEEND = as.numeric(as.character(substr(as.character(AGE),2,4)))) %>% dplyr::mutate(CORRECTAGE = ifelse(AGEFIRST==1,AGEEND,ifelse(AGEFIRST==2,AGEEND/12,ifelse(AGEFIRST==4,AGEEND/365,0)))) %>% dplyr::mutate(RENAMERACE = ifelse(RACE==1,"White",ifelse(RACE==2,"Black",ifelse(RACE==3,"Native American","Asian")))) %>% group_by(RENAMERACE) %>% summarize(AVGAGE05 = mean(CORRECTAGE)) #%>% View
  
  #dfCan2010 <- df2010 %>% dplyr::mutate(SL = substr(UCOD,1,1)) %>% dplyr::mutate(CAN = ifelse(SL=="C",1,ifelse(SL=="D",1,0))) %>% dplyr::mutate(AGEFIRST = as.numeric(as.character(substr(as.character(AGE),1,1)))) %>% dplyr::mutate(AGEEND = as.numeric(as.character(substr(as.character(AGE),2,4)))) %>% dplyr::mutate(CORRECTAGE = ifelse(AGEFIRST==1,AGEEND,ifelse(AGEFIRST==2,AGEEND/12,ifelse(AGEFIRST==4,AGEEND/365,0)))) %>% dplyr::mutate(RENAMERACE = ifelse(RACE==1,"White",ifelse(RACE==2,"Black",ifelse(RACE==3,"Native American","Asian")))) %>% group_by(RENAMERACE) %>% summarize(CP10 = mean(CAN),AVGAGE10 = mean(CORRECTAGE)) #%>% View
  
  dfCan2010 <- df2010 %>% dplyr::mutate(SL = substr(UCOD,1,1)) %>% dplyr::mutate(CAN = ifelse(SL=="C",1,ifelse(SL=="D",1,0))) %>% dplyr::mutate(AGEFIRST = as.numeric(as.character(substr(as.character(AGE),1,1)))) %>% dplyr::mutate(AGEEND = as.numeric(as.character(substr(as.character(AGE),2,4)))) %>% dplyr::mutate(CORRECTAGE = ifelse(AGEFIRST==1,AGEEND,ifelse(AGEFIRST==2,AGEEND/12,ifelse(AGEFIRST==4,AGEEND/365,0)))) %>% dplyr::mutate(RENAMERACE = ifelse(RACE==1,"White",ifelse(RACE==2,"Black",ifelse(RACE==3,"Native American","Asian")))) %>% group_by(RENAMERACE) %>% summarize(AVGAGE10 = mean(CORRECTAGE)) #%>% View
  
  #dfCan2010 <- mortal10 %>% dplyr::mutate(SL = substr(ucod,1,1)) %>% dplyr::mutate(can = ifelse(SL=="C",1,ifelse(SL=="D",1,0))) %>% group_by(racer5) %>% summarize(cp10 = mean(can)) #%>% View
  
  #dfCan2014 <- df2014 %>% dplyr::mutate(SL = substr(UCOD,1,1)) %>% dplyr::mutate(CAN = ifelse(SL=="C",1,ifelse(SL=="D",1,0))) %>% dplyr::mutate(AGEFIRST = as.numeric(as.character(substr(as.character(AGE),1,1)))) %>% dplyr::mutate(AGEEND = as.numeric(as.character(substr(as.character(AGE),2,4)))) %>% dplyr::mutate(CORRECTAGE = ifelse(AGEFIRST==1,AGEEND,ifelse(AGEFIRST==2,AGEEND/12,ifelse(AGEFIRST==4,AGEEND/365,0)))) %>% dplyr::mutate(RENAMERACE = ifelse(RACE==1,"White",ifelse(RACE==2,"Black",ifelse(RACE==3,"Native American","Asian")))) %>% group_by(RENAMERACE) %>% summarize(CP14 = mean(CAN),AVGAGE14 = mean(CORRECTAGE)) #%>% View
  
  dfCan2014 <- df2014 %>% dplyr::mutate(SL = substr(UCOD,1,1)) %>% dplyr::mutate(CAN = ifelse(SL=="C",1,ifelse(SL=="D",1,0))) %>% dplyr::mutate(AGEFIRST = as.numeric(as.character(substr(as.character(AGE),1,1)))) %>% dplyr::mutate(AGEEND = as.numeric(as.character(substr(as.character(AGE),2,4)))) %>% dplyr::mutate(CORRECTAGE = ifelse(AGEFIRST==1,AGEEND,ifelse(AGEFIRST==2,AGEEND/12,ifelse(AGEFIRST==4,AGEEND/365,0)))) %>% dplyr::mutate(RENAMERACE = ifelse(RACE==1,"White",ifelse(RACE==2,"Black",ifelse(RACE==3,"Native American","Asian")))) %>% group_by(RENAMERACE) %>% summarize(AVGAGE14 = mean(CORRECTAGE)) #%>% View
  
  #dfCan2014 <- mortal14 %>% dplyr::mutate(SL = substr(ucod,1,1)) %>% dplyr::mutate(can = ifelse(SL=="C",1,ifelse(SL=="D",1,0))) %>% group_by(racer5) %>% summarize(cp14 = mean(can)) #%>% View
    #%>% group_by(racer5) %>% summarize(canPerc = mean(can)) %>% View
  
  dfCanComb1 <- dplyr::inner_join(dfCan2005,dfCan2010,by="RENAMERACE")
  dfCanComb2 <- dplyr::inner_join(dfCanComb1,dfCan2014,by="RENAMERACE") #%>% View
  
  mdfCancer <- melt(dfCanComb2,id="RENAMERACE") #%>% View #%>% arrange(RENAMERACE) %>% View
  
  output$cancerRace <- renderPlot({
    # cr <- ggplot() +
    #   coord_cartesian() +
    #   theme_gray() +
    #   layer(data=mdfCancer,
    #         geom="line",
    #         mapping = aes(x=variable,y=value,fill=RENAMERACE),
    #         #mapping = aes(x=SL,y=ifelse(YEAR==2005,N05B,ifelse(YEAR==2010,N10B,N14B))),
    #         stat = "identity",
    #         position = position_identity())
    # cr
    
    cr <- ggplot(mdfCancer, aes(variable,value,color=RENAMERACE, group=RENAMERACE)) + geom_point()  + geom_line() + scale_x_discrete(labels=c("AVGAGE05" = "2005", "AVGAGE10" = "2010","AVGAGE14" = "2014")) + labs(x="Year",y="Average Age at Cancer Death") + labs(title="Time Series - Inequality in Healthcare Improvement") + labs(color="Race")
    #+ 
      #scale_color_manual(values = c("p1" = "red", "p2" = "blue", "p3" = "green")) + geom_line()
    cr
  
  })
  
  # Show that married and divorced people are more likely to die from cancer than single or widowed - Good
  
  dfMar05 <- df2005 %>% filter(EDUC89!=99) %>% filter(MARSTAT!="U") %>% filter(EDUC89!=0) %>% dplyr::mutate(SL = substr(UCOD,1,1)) %>% dplyr::mutate(CAN = ifelse(SL=="C",1,ifelse(SL=="D",1,0))) %>% dplyr::mutate(EDUCREFORMAT = ifelse(as.numeric(as.character(EDUC89))<9, "01 <8th Grade", ifelse(as.numeric(as.character(EDUC89))<12, "02 Less than Diploma",ifelse(as.numeric(as.character(EDUC89))<16,"03 Some college","04 College Grad")))) %>% dplyr::mutate(MARADJ = ifelse(MARSTAT=="D","Divorced",ifelse(MARSTAT=="M","Married",ifelse(MARSTAT=="S","Single",ifelse(MARSTAT=="W","Widowed","Unknown"))))) %>% group_by(EDUCREFORMAT,MARADJ) %>% summarize(CANCER05 = mean(CAN)) %>% dplyr::mutate(CATEGORY = ifelse(CANCER05>0.21, "High", "Low")) #%>% View
  
  dfMar10 <- df2010 %>% filter(EDUC2003!=99) %>% filter(MARSTAT!="U") %>% filter(EDUC2003!=0) %>% dplyr::mutate(SL = substr(UCOD,1,1)) %>% dplyr::mutate(CAN = ifelse(SL=="C",1,ifelse(SL=="D",1,0))) %>% dplyr::mutate(EDUC = ifelse(as.numeric(as.character(EDUC2003))==1, "01 Less than HS", ifelse(as.numeric(as.character(EDUC2003))==2, "02 No diploma", ifelse(as.numeric(as.character(EDUC2003))==3, "03 HS Diploma", ifelse(as.numeric(as.character(EDUC2003))==4, "04 Some college", ifelse(as.numeric(as.character(EDUC2003))==5, "05 Associate's", ifelse(as.numeric(as.character(EDUC2003))==6, "06 Bachelor's", ifelse(as.numeric(as.character(EDUC2003))==7, "07 Master's", "08 Doctorate")))))))) %>% dplyr::mutate(MARADJ = ifelse(MARSTAT=="D","Divorced",ifelse(MARSTAT=="M","Married",ifelse(MARSTAT=="S","Single",ifelse(MARSTAT=="W","Widowed","Unknown"))))) %>% group_by(EDUC,MARADJ) %>% summarize(CANCER10 = mean(CAN)) %>% dplyr::mutate(CATEGORY = ifelse(CANCER10>0.21, "High", "Low")) #%>% View
  
  dfMar14 <- df2014 %>% filter(EDUC2003!=99) %>% filter(MARSTAT!="U") %>% filter(EDUC2003!=0) %>% dplyr::mutate(SL = substr(UCOD,1,1)) %>% dplyr::mutate(CAN = ifelse(SL=="C",1,ifelse(SL=="D",1,0))) %>% dplyr::mutate(EDUC = ifelse(as.numeric(as.character(EDUC2003))==1, "01 Less than HS", ifelse(as.numeric(as.character(EDUC2003))==2, "02 No diploma", ifelse(as.numeric(as.character(EDUC2003))==3, "03 HS Diploma", ifelse(as.numeric(as.character(EDUC2003))==4, "04 Some college", ifelse(as.numeric(as.character(EDUC2003))==5, "05 Associate's", ifelse(as.numeric(as.character(EDUC2003))==6, "06 Bachelor's", ifelse(as.numeric(as.character(EDUC2003))==7, "07 Master's", "08 Doctorate")))))))) %>% dplyr::mutate(MARADJ = ifelse(MARSTAT=="D","Divorced",ifelse(MARSTAT=="M","Married",ifelse(MARSTAT=="S","Single",ifelse(MARSTAT=="W","Widowed","Unknown"))))) %>% group_by(EDUC,MARADJ) %>% summarize(CANCER14 = mean(CAN)) %>% dplyr::mutate(CATEGORY = ifelse(CANCER14>0.21, "High", "Low")) #%>% View
  
  observeEvent(input$y5,{ 
  output$marCrosstab <- renderPlot({
    mc <- ggplot() +
      coord_cartesian() +
      scale_fill_manual(values=c("red","green4")) +
      labs(x="Level of Education",y="Marital Status") +
      labs(title="Effect of Personal Circumstances on Cancer Deaths - 2005") +
      labs(fill="Cancer Death Percentage") +
      layer(data=dfMar05,
            geom="tile",
            mapping=aes(x=EDUCREFORMAT,y=MARADJ,fill=CATEGORY),
            stat = "identity",
            position=position_identity()) +
      layer(data=dfMar05,
            geom="text",
            mapping=aes(x=EDUCREFORMAT,y=MARADJ,label=round(CANCER05,4)),
            stat = "identity",
            position=position_identity())
    mc
  })
  })
  
  observeEvent(input$y10,{ 
  output$marCrosstab <- renderPlot({
    mc2 <- ggplot() +
      coord_cartesian() +
      scale_fill_manual(values=c("red","green4")) +
      labs(x="Level of Education",y="Marital Status") +
      labs(title="Effect of Personal Circumstances on Cancer Deaths - 2010") +
      labs(fill="Cancer Death Percentage") +
      layer(data=dfMar10,
            geom="tile",
            mapping=aes(x=EDUC,y=MARADJ,fill=CATEGORY),
            stat = "identity",
            position=position_identity()) +
      layer(data=dfMar10,
            geom="text",
            mapping=aes(x=EDUC,y=MARADJ,label=round(CANCER10,4)),
            stat = "identity",
            position=position_identity())
    mc2
  })
  })
  
  observeEvent(input$y14,{ 
  output$marCrosstab <- renderPlot({
    mc3 <- ggplot() +
      coord_cartesian() +
      scale_fill_manual(values=c("red","green4")) +
      labs(x="Level of Education",y="Marital Status") +
      labs(title="Effect of Personal Circumstances on Cancer Deaths - 2014") +
      labs(fill="Cancer Death Percentage") +
      layer(data=dfMar14,
            geom="tile",
            mapping=aes(x=EDUC,y=MARADJ,fill=CATEGORY),
            stat = "identity",
            position=position_identity()) +
      layer(data=dfMar14,
            geom="text",
            mapping=aes(x=EDUC,y=MARADJ,label=round(CANCER14,4)),
            stat = "identity",
            position=position_identity())
    mc3
  })
  })
  
})

shinyApp(ui = ui, server = server)
